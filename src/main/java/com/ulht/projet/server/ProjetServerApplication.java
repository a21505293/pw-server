package com.ulht.projet.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetServerApplication.class, args);
	}

}
